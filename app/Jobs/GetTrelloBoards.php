<?php

namespace App\Jobs;

use App\Business\Data\Wizard\Wizard;
use Exception;
use Illuminate\Support\Facades\Redis;

class GetTrelloBoards extends Job
{
    /**
     * Название организации
     *
     * @var string
     */
    public $organisation;

    /**
     * Настройщик
     *
     * @var \App\Business\Data\Wizard\Wizard
     */
    protected $wizard;

    /**
     * Создает экземпляр джоба
     *
     * @param  string  $organisation
     */
    public function __construct(string $organisation)
    {
        $this->organisation = $organisation;
    }

    /**
     * Инициализации перед выполнением джоба
     *
     * @return void
     */
    protected function startup()
    {
        parent::startup();

        $this->wizard = app()->make(Wizard::class);
    }

    /**
     * Выполняет джоб, до того, как происходит логика завершения выполнения
     *
     * @return void
     */
    protected function perform()
    {
        $this->wizard->endGetBoards($this->organisation);
    }

    /**
     * Действия перед основной обработкой ошибок
     *
     * @param  \Exception  $ex
     * @return void
     */
    protected function preFail(Exception $ex)
    {
        Redis::publish('trello', json_encode([
            'key' => $this->organisation,
            'exception' => get_class($ex),
            'message' => $ex->getMessage(),
        ]));
    }
}
