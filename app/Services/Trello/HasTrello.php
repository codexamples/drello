<?php

namespace App\Services\Trello;

use App\Business\Services\Trello\Trello;

/**
 * Класс, имеющий обертку над Trello-библиотекой, как синглтон
 */
trait HasTrello
{
    /**
     * Обертка над Trello-библиотекой
     *
     * @var \App\Business\Services\Trello\Trello
     */
    private $trello;

    /**
     * Возвращает обертку над Trello-библиотекой
     *
     * @return \App\Business\Services\Trello\Trello
     */
    protected function trello()
    {
        if ($this->trello === null) {
            $this->trello = app()->make(Trello::class);
        }

        return $this->trello;
    }
}
