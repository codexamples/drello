<?php

namespace App\Services\Trello;

use App\Business\Services\Trello\Trello as TrelloContract;
use App\Models\Preferences\HasSettings;
use App\Services\Trello\Webhook\Handler;
use App\Services\Trello\Webhook\HandlerNotFoundException;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Trello\Client;
use Trello\Manager;

/**
 * Реализация обертки над Trello-библиотекой
 */
class Trello implements TrelloContract
{
    use HasSettings;

    /**
     * Клиент Trello
     *
     * @var \Trello\Client
     */
    protected $client;

    /**
     * Манагер клиента Trello
     *
     * @var \Trello\Manager
     */
    protected $manager;

    /**
     * Возвращает или задает значение поля в настройках
     *
     * @param  string  $section
     * @param  string  $key
     * @param  $value
     * @return mixed|static
     */
    protected function propFromSettings(string $section, string $key, $value = null)
    {
        if ($value === null) {
            return $this->settings()->loadSection($section)->read($key);
        }

        $this->settings()->saveSection(
            $this->settings()->loadSection($section)->write($key, $value)
        );

        return $this;
    }

    /**
     * Возвращает или задает ключ от Trello Api
     *
     * @param  $value
     * @return string|static
     */
    public function apiKey($value = null)
    {
        return $this->propFromSettings('trello', 'key', $value);
    }

    /**
     * Возвращает или задает токен от Trello Api
     *
     * @param  $value
     * @return string|static
     */
    public function apiToken($value = null)
    {
        return $this->propFromSettings('trello', 'token', $value);
    }

    /**
     * Возвращает клиента Trello
     *
     * @return \Trello\Client
     */
    public function client()
    {
        if ($this->client === null) {
            $this->client = new Client();
            $this->client->authenticate($this->apiKey(), $this->apiToken(), Client::AUTH_URL_CLIENT_ID);
        }

        return $this->client;
    }

    /**
     * Возвращает манагера клиента Trello
     *
     * @return \Trello\Manager
     */
    public function manager()
    {
        if ($this->manager === null) {
            $this->manager = new Manager($this->client());
        }

        return $this->manager;
    }

    /**
     * Возвращает вебхук(-и), подвязанные на токен
     *
     * @param  string  $id
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function getWebhooks(string $id = '')
    {
        try {
            return $this->client()->tokens()->webhooks()->show($this->apiToken(), $id);
        } catch (Exception $ex) {
            throw new HttpException(400, $ex->getMessage(), $ex);
        }
    }

    /**
     * Создает вебхук
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function setWebhook(Request $request)
    {
        try {
            if (!$request->input('description')) {
                $request->merge(['description' => 'Auth-generated at ' . date('Y-m-d H:i:s')]);
            }

            if (!$request->input('callbackURL')) {
                $request->merge(['callbackURL' => url('/api/v1/webhook')]);
            }

            return $this->client()->tokens()->webhooks()->create($this->apiToken(), $request->all());
        } catch (Exception $ex) {
            throw new HttpException(400, $ex->getMessage(), $ex);
        }
    }

    /**
     * Удаляет вебхук
     *
     * @param  string  $id
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function dropWebhook(string $id)
    {
        try {
            return $this->client()->tokens()->webhooks()->remove($this->apiToken(), $id);
        } catch (Exception $ex) {
            throw new HttpException(400, $ex->getMessage(), $ex);
        }
    }

    /**
     * Обрабатывает вебхуки, сгенеренные из Trello
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function handleWebhook(Request $request)
    {
        // anti-warning: might not been defined
        $data = null;

        try {
            $data = json_decode($request->getContent(), true)['action'];

            Handler::make($data['display']['translationKey'])->handle($data['data']);
        } catch (HandlerNotFoundException $not_found) {
            Log::info('==> ' . $not_found->getMessage());
            Log::info('Entities: ' . "\n" . json_encode($data['data'], JSON_UNESCAPED_UNICODE));

            return;
        } catch (Exception $ex) {
            try {
                Log::info('==> ' . $data['display']['translationKey']);
                Log::info('Entities: ' . "\n" . json_encode($data['data'], JSON_UNESCAPED_UNICODE));
            } catch (Exception $sub_ex) {
                Log::info('==> Error occurred');
            }

            Log::info('Class: ' . get_class($ex));
            Log::info('Message: ' . $ex->getMessage());
            Log::info('Stack: ' . "\n" . $ex->getTraceAsString());
        }
    }
}
