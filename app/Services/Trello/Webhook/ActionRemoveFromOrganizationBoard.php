<?php

namespace App\Services\Trello\Webhook;

/**
 * Действие: удаление доски из организации
 */
class ActionRemoveFromOrganizationBoard extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $this->models('board')->destroy($entities['board']['id']);
    }
}
