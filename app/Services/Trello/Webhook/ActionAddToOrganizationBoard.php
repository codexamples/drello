<?php

namespace App\Services\Trello\Webhook;

use Illuminate\Http\Request;

/**
 * Действие: добавление доски в организацию
 */
class ActionAddToOrganizationBoard extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $this->models('organisation')->one($entities['organization']['id']);

        $data = $this->trello()->manager()->getBoard($entities['board']['id'])->getData();
        $request = new Request();
        $request->merge([
            '_id' => $data['id'],
            'name' => $data['name'],
            'url' => $data['url'],
            'closed' => $data['closed'],
            'organisation_id' => $entities['organization']['id'],
        ]);

        $this->models('board')->create($request);
    }
}
