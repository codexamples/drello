<?php

namespace App\Services\Trello\Webhook;

/**
 * Действие: смена краткого названия у организации
 */
class ActionChangedNameOfOrganization extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $organisation = $this->models('organisation')->one($entities['organization']['id']);

        $organisation->name = $entities['organization']['name'];
        $organisation->save();
    }
}
