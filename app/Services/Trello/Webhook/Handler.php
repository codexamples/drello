<?php

namespace App\Services\Trello\Webhook;

use App\Business\Repositories\ManyToManyRepository;
use App\Services\Trello\HasTrello;

/**
 * Обработчик вебхуков, сгенеренных из Trello
 */
abstract class Handler
{
    use HasTrello;

    /**
     * Репозитории моделей
     *
     * @var array
     */
    private $repositories = [];

    /**
     * Обработчики вебхуков, сгенеренных из Trello
     *
     * @var array
     */
    private static $handlers = [];

    /**
     * Возвращает репозиторий из спец.массива
     *
     * @param  string  $key
     * @return mixed
     */
    private function repositoryFromArray(string $key)
    {
        if (!array_key_exists($key, $this->repositories)) {
            $this->repositories[$key] = app()->make(
                $key === 'many'
                    ? ManyToManyRepository::class
                    : '\App\Models\Repositories\\' . studly_case($key . '_repository')
            );
        }

        return $this->repositories[$key];
    }

    /**
     * Возвращает репозиторий связей "многие-ко-многим"
     *
     * @return \App\Business\Repositories\ManyToManyRepository
     */
    protected function manyToMany()
    {
        return $this->repositoryFromArray('many');
    }

    /**
     * Возвращает репозиторий моделей
     *
     * @param  string  $name
     * @return \App\Business\Repositories\ModelRepository
     */
    protected function models(string $name)
    {
        return $this->repositoryFromArray($name);
    }

    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    abstract public function handle(array $entities);

    /**
     * Создает и возвращает обработчик, исходя из названия операции
     *
     * @param  string  $translation_key
     * @return static
     * @throws \App\Services\Trello\Webhook\HandlerNotFoundException
     */
    final public static function make(string $translation_key)
    {
        if (!array_key_exists($translation_key, static::$handlers)) {
            $class = '\App\Services\Trello\Webhook\\' . studly_case($translation_key);

            if (class_exists($class, true)) {
                static::$handlers[$translation_key] = app()->make($class);
            } else {
                throw new HandlerNotFoundException($translation_key);
            }
        }

        return static::$handlers[$translation_key];
    }
}
