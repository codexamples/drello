<?php

namespace App\Services\Trello\Webhook;

/**
 * Действие: закрытие доски
 */
class ActionClosedBoard extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $board = $this->models('board')->one($entities['board']['id']);

        $board->closed = true;
        $board->save();
    }
}
