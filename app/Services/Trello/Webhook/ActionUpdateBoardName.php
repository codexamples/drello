<?php

namespace App\Services\Trello\Webhook;

/**
 * Действие: обновление названия у доски
 */
class ActionUpdateBoardName extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $board = $this->models('board')->one($entities['board']['id']);

        $board->name = $entities['board']['name'];
        $board->save();
    }
}
