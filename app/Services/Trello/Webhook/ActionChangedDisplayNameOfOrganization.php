<?php

namespace App\Services\Trello\Webhook;

/**
 * Действие: смена полного названия у организации
 */
class ActionChangedDisplayNameOfOrganization extends Handler
{
    /**
     * Обрабатывает вебхук, сгенеренный из Trello
     *
     * @param  array  $entities
     * @return void
     */
    public function handle(array $entities)
    {
        $organisation = $this->models('organisation')->one($entities['organization']['id']);

        $organisation->display_name = $entities['organization']['displayName'];
        $organisation->save();
    }
}
