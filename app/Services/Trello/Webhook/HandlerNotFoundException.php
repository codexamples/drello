<?php

namespace App\Services\Trello\Webhook;

use Exception;
use Throwable;

/**
 * Обработчик вебхука, сгенеренного из Trello, не найден
 */
class HandlerNotFoundException extends Exception
{
    /**
     * Создает новый экземпляр исключения
     *
     * @param  string  $translation_key
     * @param  int  $code
     * @param  \Throwable  $previous
     */
    public function __construct(string $translation_key, int $code = 0, Throwable $previous = null)
    {
        parent::__construct('Handler not found: ' . $translation_key, $code, $previous);
    }
}
