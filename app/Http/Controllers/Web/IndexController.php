<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Главная страница a.k.a. React-Based
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index', [
            'hash' => [
                'css' => md5_file(public_path('css/index.css')),
                'js' => md5_file(public_path('bundle.js')),
            ],
        ]);
    }
}
