<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\CardCrudRequest;
use App\Models\Repositories\CardRepository;
use Illuminate\Http\Request;

class CardController extends Controller
{
    /**
     * Репозиторий карточек
     *
     * @var \App\Models\Repositories\CardRepository
     */
    private $cards;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\CardRepository  $cards
     */
    public function __construct(CardRepository $cards)
    {
        $this->cards = $cards;
    }

    /**
     * Возвращает репозиторий карточек
     *
     * @return \App\Models\Repositories\CardRepository
     */
    protected function cards()
    {
        return $this->cards;
    }

    /**
     * Список всех карточек
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->cards()->all($request));
    }

    /**
     * Создание новой карточки
     *
     * @param  \App\Http\Requests\Api\VersionOne\CardCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CardCrudRequest $request)
    {
        return response()->json($this->cards()->create($request));
    }

    /**
     * Данные карточки
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->cards()->one($id));
    }

    /**
     * Обновление карточки
     *
     * @param  \App\Http\Requests\Api\VersionOne\CardCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CardCrudRequest $request, string $id)
    {
        return response()->json($this->cards()->update($id, $request));
    }

    /**
     * Удаление карточки
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->cards()->destroy($id));
    }
}
