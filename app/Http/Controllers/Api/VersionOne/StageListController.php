<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\StageListCrudRequest;
use App\Models\Repositories\CardRepository;
use App\Models\Repositories\StageListRepository;
use Illuminate\Http\Request;

class StageListController extends Controller
{
    /**
     * Репозиторий стадиевых списков
     *
     * @var \App\Models\Repositories\StageListRepository
     */
    private $stageLists;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\StageListRepository  $stageLists
     */
    public function __construct(StageListRepository $stageLists)
    {
        $this->stageLists = $stageLists;
    }

    /**
     * Возвращает репозиторий стадиевых списков
     *
     * @return \App\Models\Repositories\StageListRepository
     */
    protected function stageLists()
    {
        return $this->stageLists;
    }

    /**
     * Список всех стадиевых списков
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->stageLists()->all($request));
    }

    /**
     * Создание нового стадиевого списка
     *
     * @param  \App\Http\Requests\Api\VersionOne\StageListCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StageListCrudRequest $request)
    {
        return response()->json($this->stageLists()->create($request));
    }

    /**
     * Данные стадиевого списка
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->stageLists()->one($id));
    }

    /**
     * Обновление стадиевого списка
     *
     * @param  \App\Http\Requests\Api\VersionOne\StageListCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StageListCrudRequest $request, string $id)
    {
        return response()->json($this->stageLists()->update($id, $request));
    }

    /**
     * Удаление стадиевого списка
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->stageLists()->destroy($id));
    }

    /**
     * Список дочерних карточек у стадиевого списка
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @param  \App\Models\Repositories\CardRepository  $cards
     * @return \Illuminate\Http\Response
     */
    public function cards(Request $request, string $id, CardRepository $cards)
    {
        $stage_list = $this->stageLists()->one($id);
        $request->merge(['stage_list_id' => $stage_list->id]);

        return response()->json($cards->all($request));
    }
}
