<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\OrganisationCrudRequest;
use App\Models\Repositories\BoardRepository;
use App\Models\Repositories\OrganisationRepository;
use Illuminate\Http\Request;

class OrganisationController extends Controller
{
    /**
     * Репозиторий организаций
     *
     * @var \App\Models\Repositories\OrganisationRepository
     */
    private $organisations;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\OrganisationRepository  $organisations
     */
    public function __construct(OrganisationRepository $organisations)
    {
        $this->organisations = $organisations;
    }

    /**
     * Возвращает репозиторий организаций
     *
     * @return \App\Models\Repositories\OrganisationRepository
     */
    protected function organisations()
    {
        return $this->organisations;
    }

    /**
     * Список всех организаций
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->organisations()->all($request));
    }

    /**
     * Создание новой организации
     *
     * @param  \App\Http\Requests\Api\VersionOne\OrganisationCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrganisationCrudRequest $request)
    {
        return response()->json($this->organisations()->create($request));
    }

    /**
     * Данные организации
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->organisations()->one($id));
    }

    /**
     * Обновление организации
     *
     * @param  \App\Http\Requests\Api\VersionOne\OrganisationCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrganisationCrudRequest $request, string $id)
    {
        return response()->json($this->organisations()->update($id, $request));
    }

    /**
     * Удаление организации
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->organisations()->destroy($id));
    }

    /**
     * Список дочерних досок у организации
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @param  \App\Models\Repositories\BoardRepository  $boards
     * @return \Illuminate\Http\Response
     */
    public function boards(Request $request, string $id, BoardRepository $boards)
    {
        $organisation = $this->organisations()->one($id);
        $request->merge(['organisation_id' => $organisation->id]);

        return response()->json($boards->all($request));
    }
}
