<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\AuthLoginRequest;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Авторизация пользователя
     *
     * @param  \App\Http\Requests\Api\VersionOne\AuthLoginRequest  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function login(AuthLoginRequest $request)
    {
        if (Auth::attempt($request->all())) {
            /** @var  \App\Business\Authorization\ApiTokenAccessible  $user */

            $user = Auth::user();
            $token = $user->updateApiToken();

            return response()->json([
                'user' => $user,
                'token' => $token,
            ]);
        }

        throw new AuthenticationException('Invalid credentials');
    }

    /**
     * Разлогинивание пользователя
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        /** @var  \App\Business\Authorization\ApiTokenAccessible  $user */

        $user = Auth::guard('api')->user();
        $user->dropApiToken();

        return response()->json(['user' => $user]);
    }
}
