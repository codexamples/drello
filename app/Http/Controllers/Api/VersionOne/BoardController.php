<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\BoardCrudRequest;
use App\Models\Repositories\BoardRepository;
use App\Models\Repositories\LabelRepository;
use App\Models\Repositories\StageListRepository;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    /**
     * Репозиторий досок
     *
     * @var \App\Models\Repositories\BoardRepository
     */
    private $boards;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\BoardRepository  $boards
     */
    public function __construct(BoardRepository $boards)
    {
        $this->boards = $boards;
    }

    /**
     * Возвращает репозиторий досок
     *
     * @return \App\Models\Repositories\BoardRepository
     */
    protected function boards()
    {
        return $this->boards;
    }

    /**
     * Список всех досок
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->boards()->all($request));
    }

    /**
     * Создание новой доски
     *
     * @param  \App\Http\Requests\Api\VersionOne\BoardCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BoardCrudRequest $request)
    {
        return response()->json($this->boards()->create($request));
    }

    /**
     * Данные доски
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->boards()->one($id));
    }

    /**
     * Обновление доски
     *
     * @param  \App\Http\Requests\Api\VersionOne\BoardCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BoardCrudRequest $request, string $id)
    {
        return response()->json($this->boards()->update($id, $request));
    }

    /**
     * Удаление доски
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->boards()->destroy($id));
    }

    /**
     * Список дочерних стадиевых списков у доски
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @param  \App\Models\Repositories\StageListRepository  $stage_lists
     * @return \Illuminate\Http\Response
     */
    public function stageLists(Request $request, string $id, StageListRepository $stage_lists)
    {
        $board = $this->boards()->one($id);
        $request->merge(['board_id' => $board->id]);

        return response()->json($stage_lists->all($request));
    }

    /**
     * Список дочерних стадиевых списков у доски
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @param  \App\Models\Repositories\LabelRepository  $labels
     * @return \Illuminate\Http\Response
     */
    public function labels(Request $request, string $id, LabelRepository $labels)
    {
        $board = $this->boards()->one($id);
        $request->merge(['board_id' => $board->id]);

        return response()->json($labels->all($request));
    }
}
