<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Services\Trello\HasTrello;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    use HasTrello;

    /**
     * Проверка вебхука из Trello
     *
     * @return \Illuminate\Http\Response
     */
    public function check()
    {
        return response()->make('ok');
    }

    /**
     * Обработчик вебхука из Trello
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function handle(Request $request)
    {
        $this->trello()->handleWebhook($request);

        return response()->make('ok');
    }
}
