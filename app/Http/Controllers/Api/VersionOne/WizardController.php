<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Business\Data\Wizard\Wizard;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\WizardCredentialsRequest;
use App\Http\Requests\Api\VersionOne\WizardGetBoardsRequest;
use App\Http\Requests\Api\VersionOne\WizardSetBoardsRequest;

class WizardController extends Controller
{
    /**
     * Настройщик
     *
     * @var \App\Business\Data\Wizard\Wizard
     */
    private $wizard;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Business\Data\Wizard\Wizard  $wizard
     */
    public function __construct(Wizard $wizard)
    {
        $this->wizard = $wizard;
    }

    /**
     * Возвращает настройщика
     *
     * @return \App\Business\Data\Wizard\Wizard
     */
    protected function wizard()
    {
        return $this->wizard;
    }

    /**
     * Проверка готовности настроек
     *
     * @return \Illuminate\Http\Response
     */
    public function checkReady()
    {
        return response()->json($this->wizard()->determineReadyState());
    }

    /**
     * Возвращает авторизационные реквизиты
     *
     * @return \Illuminate\Http\Response
     */
    public function getCredentials()
    {
        return response()->json([
            'key' => $this->wizard()->getApiKey(),
            'token' => $this->wizard()->getApiToken(),
        ]);
    }

    /**
     * Задает авторизационные реквизиты
     *
     * @param  \App\Http\Requests\Api\VersionOne\WizardCredentialsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function setCredentials(WizardCredentialsRequest $request)
    {
        $this->wizard()->setCredentials($request);

        return response()->json(true);
    }

    /**
     * Стартует джоб на выборку всех данных
     *
     * @param  \App\Http\Requests\Api\VersionOne\WizardGetBoardsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function getBoards(WizardGetBoardsRequest $request)
    {
        return response()->json($this->wizard()->getBoards($request));
    }

    /**
     * Подверждает заполнение ранее выбранными моделями
     *
     * @param  \App\Http\Requests\Api\VersionOne\WizardSetBoardsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function setBoards(WizardSetBoardsRequest $request)
    {
        return response()->json($this->wizard()->setBoards($request));
    }
}
