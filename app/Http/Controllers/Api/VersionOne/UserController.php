<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\UserCrudRequest;
use App\Http\Requests\Api\VersionOne\UserPasswordForcedRequest;
use App\Http\Requests\Api\VersionOne\UserPasswordRequest;
use App\Models\Repositories\UserRepository;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Репозиторий пользователей
     *
     * @var \App\Models\Repositories\UserRepository
     */
    private $users;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\UserRepository  $users
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Возвращает репозиторий пользователей
     *
     * @return \App\Models\Repositories\UserRepository
     */
    protected function users()
    {
        return $this->users;
    }

    /**
     * Список всех пользователей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->users()->all($request));
    }

    /**
     * Создание нового пользователя
     *
     * @param  \App\Http\Requests\Api\VersionOne\UserCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCrudRequest $request)
    {
        return response()->json($this->users()->create($request));
    }

    /**
     * Данные пользователя
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(int $id)
    {
        return response()->json($this->users()->one($id));
    }

    /**
     * Обновление пользователя
     *
     * @param  \App\Http\Requests\Api\VersionOne\UserCrudRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserCrudRequest $request, int $id)
    {
        return response()->json($this->users()->update($id, $request));
    }

    /**
     * Удаление пользователя
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        return response()->json($this->users()->destroy($id));
    }

    /**
     * Смена пароля у пользователя
     *
     * @param  \App\Http\Requests\Api\VersionOne\UserPasswordRequest  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function password(UserPasswordRequest $request)
    {
        /** @var  \App\Business\Authorization\PasswordAccessible  $user */
        $user = Auth::guard('api')->user();

        if ($user->passwordAsserts($request->input('old_password'))) {
            $user->changePassword($request->input('password'));

            return response()->json(['user' => $user]);
        }

        throw new AuthenticationException('The old password is wrong');
    }

    /**
     * Смена пароля у пользователя
     *
     * @param  \App\Http\Requests\Api\VersionOne\UserPasswordForcedRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function passwordForced(UserPasswordForcedRequest $request, int $id)
    {
        /** @var  \App\Business\Authorization\PasswordAccessible  $user */
        $user = $this->users()->one($id);

        $user->changePassword($request->input('password'));

        return response()->json(['user' => $user]);
    }
}
