<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\LabelCrudRequest;
use App\Models\Repositories\LabelRepository;
use Illuminate\Http\Request;

class LabelController extends Controller
{
    /**
     * Репозиторий этикеток
     *
     * @var \App\Models\Repositories\LabelRepository
     */
    private $labels;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\LabelRepository  $labels
     */
    public function __construct(LabelRepository $labels)
    {
        $this->labels = $labels;
    }

    /**
     * Возвращает репозиторий этикеток
     *
     * @return \App\Models\Repositories\LabelRepository
     */
    protected function labels()
    {
        return $this->labels;
    }

    /**
     * Список всех этикеток
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->labels()->all($request));
    }

    /**
     * Создание новой этикетки
     *
     * @param  \App\Http\Requests\Api\VersionOne\LabelCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LabelCrudRequest $request)
    {
        return response()->json($this->labels()->create($request));
    }

    /**
     * Данные этикетки
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->labels()->one($id));
    }

    /**
     * Обновление этикетки
     *
     * @param  \App\Http\Requests\Api\VersionOne\LabelCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LabelCrudRequest $request, string $id)
    {
        return response()->json($this->labels()->update($id, $request));
    }

    /**
     * Удаление этикетки
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->labels()->destroy($id));
    }
}
