<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Business\Repositories\ManyToManyRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\ManyToManyRequest;
use App\Http\Requests\Api\VersionOne\ManyToManySyncRequest;

class ManyToManyController extends Controller
{
    /**
     * Репозиторий связей "многие-ко-многим"
     *
     * @var \App\Business\Repositories\ManyToManyRepository
     */
    private $many2many;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Business\Repositories\ManyToManyRepository  $many2many
     */
    public function __construct(ManyToManyRepository $many2many)
    {
        $this->many2many = $many2many;
    }

    /**
     * Возвращает репозиторий связей "многие-ко-многим"
     *
     * @return \App\Business\Repositories\ManyToManyRepository
     */
    protected function many2many()
    {
        return $this->many2many;
    }

    /**
     * Список привязанных моделей
     *
     * @param  \App\Http\Requests\Api\VersionOne\ManyToManyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ManyToManyRequest $request)
    {
        return response()->json($this->many2many()->get($request));
    }

    /**
     * Синхронизация привязанных моделей
     *
     * @param  \App\Http\Requests\Api\VersionOne\ManyToManySyncRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function sync(ManyToManySyncRequest $request)
    {
        return response()->json($this->many2many()->sync($request));
    }
}
