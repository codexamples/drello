<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\WebhookCrudRequest;
use App\Services\Trello\HasTrello;

class WebhookModelController extends Controller
{
    use HasTrello;

    /**
     * Список вебхуков
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->trello()->getWebhooks());
    }

    /**
     * Информация о вебхуке
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->trello()->getWebhooks($id));
    }

    /**
     * Добавление нового вебхука
     *
     * @param  \App\Http\Requests\Api\VersionOne\WebhookCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebhookCrudRequest $request)
    {
        return response()->json($this->trello()->setWebhook($request));
    }

    /**
     * Удаление вебхука
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->trello()->dropWebhook($id));
    }
}
