<?php

namespace App\Http\Controllers\Api\VersionOne;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VersionOne\MemberCrudRequest;
use App\Models\Repositories\MemberRepository;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Репозиторий пользоватей Trello
     *
     * @var \App\Models\Repositories\MemberRepository
     */
    private $members;

    /**
     * Создает экземпляр контроллера
     *
     * @param  \App\Models\Repositories\MemberRepository  $members
     */
    public function __construct(MemberRepository $members)
    {
        $this->members = $members;
    }

    /**
     * Возвращает репозиторий пользоватей Trello
     *
     * @return \App\Models\Repositories\MemberRepository
     */
    protected function members()
    {
        return $this->members;
    }

    /**
     * Список всех пользоватей Trello
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($this->members()->all($request));
    }

    /**
     * Создание нового пользователя Trello
     *
     * @param  \App\Http\Requests\Api\VersionOne\MemberCrudRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberCrudRequest $request)
    {
        return response()->json($this->members()->create($request));
    }

    /**
     * Данные пользователя Trello
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function view(string $id)
    {
        return response()->json($this->members()->one($id));
    }

    /**
     * Обновление пользователя Trello
     *
     * @param  \App\Http\Requests\Api\VersionOne\MemberCrudRequest  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberCrudRequest $request, string $id)
    {
        return response()->json($this->members()->update($id, $request));
    }

    /**
     * Удаление пользователя Trello
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        return response()->json($this->members()->destroy($id));
    }
}
