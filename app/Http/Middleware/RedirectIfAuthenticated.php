<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if ($guard === 'api') {
                $user = Auth::guard($guard)->user();

                return response()->json([
                    'user' => $user,
                    'token' => $user->api_token,
                ], 406);
            }

            return redirect()->to('/');
        }

        return $next($request);
    }
}
