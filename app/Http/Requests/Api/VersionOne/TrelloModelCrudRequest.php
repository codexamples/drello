<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class TrelloModelCrudRequest extends Request
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:32',
            'url' => 'required|max:191|url',
        ];

        if (!$this->id) {
            $rules['_id'] = 'required|max:64|unique:' . $this->tableName . ',id';
        }

        return $rules;
    }
}
