<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class WizardBoardsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'organisation' => 'required|max:127',
        ];
    }
}
