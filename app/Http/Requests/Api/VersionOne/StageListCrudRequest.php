<?php

namespace App\Http\Requests\Api\VersionOne;

class StageListCrudRequest extends BasedBoardCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'stage_lists';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['closed'] = 'required|integer|between:0,1';
        $rules['position'] = 'required|numeric';

        return $rules;
    }
}
