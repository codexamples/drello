<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class UserPasswordForcedRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:4|confirmed',
        ];
    }
}
