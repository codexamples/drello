<?php

namespace App\Http\Requests\Api\VersionOne;

class BasedBoardCrudRequest extends TrelloModelCrudRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['url']);

        $rules['board_id'] = 'required|exists:boards,id';
        return $rules;
    }
}
