<?php

namespace App\Http\Requests\Api\VersionOne;

class OrganisationCrudRequest extends TrelloModelCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'organisations';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['display_name'] = 'required|max:32';

        return $rules;
    }
}
