<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class WebhookCrudRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'nullable|max:127',
            'idModel' => 'required|max:64',
            'callbackURL' => 'nullable|url|max:127',
        ];
    }
}
