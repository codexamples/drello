<?php

namespace App\Http\Requests\Api\VersionOne;

class BoardCrudRequest extends TrelloModelCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'boards';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['closed'] = 'required|integer|between:0,1';
        $rules['organisation_id'] = 'required|exists:organisations,id';

        return $rules;
    }
}
