<?php

namespace App\Http\Requests\Api\VersionOne;

class MemberCrudRequest extends TrelloModelCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'members';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['initials'] = 'required|max:4';
        $rules['tag'] = 'required|max:127';
        $rules['user_id'] = 'nullable|exists:users,id';

        return $rules;
    }
}
