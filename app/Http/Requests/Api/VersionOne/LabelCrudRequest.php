<?php

namespace App\Http\Requests\Api\VersionOne;

class LabelCrudRequest extends BasedBoardCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'labels';
}
