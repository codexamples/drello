<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class WizardCredentialsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'key' => 'required|max:64',
            'token' => 'required|max:127',
        ];
    }
}
