<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class ManyToManyRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model' => 'required|max:32',
            'key' => 'required|max:64',
            'relation' => 'required|max:32',
            'related_identifier' => 'required|max:32',
        ];
    }
}
