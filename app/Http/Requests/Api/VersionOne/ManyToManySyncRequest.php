<?php

namespace App\Http\Requests\Api\VersionOne;

class ManyToManySyncRequest extends ManyToManyRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['keys'] = 'nullable|array';
        $rules['keys.*'] = 'required|max:64';

        return $rules;
    }
}
