<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class UserCrudRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
                'max:16',
                'unique:users,name,' . $this->id . ',id',
                'regex:/^[A-Za-z_]+[A-Za-z0-9_]*$/',
            ],
            'email' => 'required|max:127|unique:users,email,' . $this->id . ',id',
        ];

        if (!$this->id) {
            $rules['password'] = 'required|min:4|confirmed';
        }

        return $rules;
    }
}
