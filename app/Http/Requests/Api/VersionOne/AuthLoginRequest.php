<?php

namespace App\Http\Requests\Api\VersionOne;

use App\Http\Requests\Request;

class AuthLoginRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:16',
            'password' => 'required',
        ];
    }
}
