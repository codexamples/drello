<?php

namespace App\Http\Requests\Api\VersionOne;

class CardCrudRequest extends TrelloModelCrudRequest
{
    /**
     * Название таблицы для формирования правил валидации
     *
     * @var string
     */
    protected $tableName = 'cards';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['due'] = 'required|date';
        $rules['closed'] = 'required|integer|between:0,1';
        $rules['position'] = 'required|numeric';
        $rules['stage_list_id'] = 'required|exists:stage_lists,id';

        return $rules;
    }
}
