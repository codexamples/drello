<?php

namespace App\Models;

class BasedBoard extends TrelloModel
{
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'url',
    ];

    /**
     * Board - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }
}
