<?php

namespace App\Models\Preferences;

use App\Business\Data\Preferences\Section;
use App\Business\Data\Preferences\SectionNotFoundException;
use App\Business\Data\Preferences\Settings as SettingsContract;
use Illuminate\Support\Facades\DB;
use ReflectionClass;

/**
 * Реализация доступа к секциям настроек
 */
class Settings implements SettingsContract
{
    /**
     * Название таблицы, в которой представлены значения настроек
     *
     * @var string
     */
    protected $table;

    /**
     * Название столбца, представляющего ключ секции
     *
     * @var string
     */
    protected $keyColumn;

    /**
     * Название столбца, представляющего значения секции
     *
     * @var string
     */
    protected $dataColumn;

    /**
     * Пространство имен, откуда брать классы секций
     *
     * @var string
     */
    protected $namespace;

    /**
     * Создает экземпляр
     */
    public function __construct()
    {
        if ($this->table === null) {
            $this->table = config('settings.table');
        }

        if ($this->keyColumn === null) {
            $this->keyColumn = config('settings.columns.key');
        }

        if ($this->dataColumn === null) {
            $this->dataColumn = config('settings.columns.data');
        }

        if ($this->namespace === null) {
            $reflection = new ReflectionClass(get_class($this));
            $this->namespace = $reflection->getNamespaceName();
        }
    }

    /**
     * Строит начало запроса к таблице настроек
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function queryTable()
    {
        return DB::table($this->table);
    }

    /**
     * Достраивает запрос к секции по указанному ключу
     *
     * @param  string  $key
     * @return \Illuminate\Database\Query\Builder
     */
    protected function queryKeyColumn(string $key)
    {
        return $this->queryTable()->where($this->keyColumn, $key);
    }

    /**
     * Выбирает из таблицы настроек значения секции по указанному ключу
     *
     * @param  string  $key
     * @return mixed
     */
    protected function querySectionRow(string $key)
    {
        return $this->queryKeyColumn($key)->first();
    }

    /**
     * Создает массив для заполнения очередной секции для таблицы настроек
     *
     * @param  \App\Business\Data\Preferences\Section  $section
     * @return array
     */
    protected function makeSectionRowData(Section $section)
    {
        return [
            $this->keyColumn => $section->getSectionKey(),
            $this->dataColumn => $section->compile(),
        ];
    }

    /**
     * Создает и возвращает экземпляр секции настроек по указанному ключу
     *
     * @param  string  $key
     * @param  $data
     * @return \App\Business\Data\Preferences\Section
     * @throws \App\Business\Data\Preferences\SectionNotFoundException
     */
    public function makeSection(string $key, $data)
    {
        $class = $this->namespace . '\\' . studly_case(mb_strtolower($key) . '_section');

        if (class_exists($class, true)) {
            return new $class($data);
        }

        throw new SectionNotFoundException($key);
    }

    /**
     * Загружает и возвращает экземпляр секции настроек по указанному ключу
     *
     * @param  string  $key
     * @return \App\Business\Data\Preferences\Section
     */
    public function loadSection(string $key)
    {
        $row = $this->querySectionRow($key);
        $dataCol = $this->dataColumn;

        return $this->makeSection($key, $row ? $row->$dataCol : null);
    }

    /**
     * Сохраняет значения секции по указанному ключу
     *
     * @param  \App\Business\Data\Preferences\Section  $section
     * @return static
     */
    public function saveSection(Section $section)
    {
        $row = $this->querySectionRow($section->getSectionKey());

        if ($row) {
            $this->queryKeyColumn($section->getSectionKey())->update($this->makeSectionRowData($section));
        } else {
            $this->queryTable()->insert($this->makeSectionRowData($section));
        }

        return $this;
    }

    /**
     * Удаляет все значения секции по указанному ключу
     *
     * @param  string  $key
     * @return static
     */
    public function removeSection(string $key)
    {
        $this->queryKeyColumn($key)->delete();

        return $this;
    }
}
