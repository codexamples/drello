<?php

namespace App\Models\Preferences;

/**
 * Секция настроек для взаимодействия с Trello
 */
class TrelloSection extends Section
{
    /**
     * Набор значений по умолчанию
     *
     * @var array
     */
    protected $defaults = [
        'key' => '',
        'token' => '',
    ];
}
