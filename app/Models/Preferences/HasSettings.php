<?php

namespace App\Models\Preferences;

use App\Business\Data\Preferences\Settings as SettingsContract;

/**
 * Класс, имеющий настройки, как синглтон
 */
trait HasSettings
{
    /**
     * Настройки
     *
     * @var \App\Business\Data\Preferences\Settings
     */
    private $settings;

    /**
     * Возвращает настройки
     *
     * @return \App\Business\Data\Preferences\Settings
     */
    protected function settings()
    {
        if ($this->settings === null) {
            $this->settings = app()->make(SettingsContract::class);
        }

        return $this->settings;
    }
}
