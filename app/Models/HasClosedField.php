<?php

namespace App\Models;

trait HasClosedField
{
    /**
     * Возвращает значение поля в булевом виде
     *
     * @return bool
     */
    public function getClosedAttribute()
    {
        return boolval($this->attributes['closed']);
    }
}
