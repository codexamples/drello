<?php

namespace App\Models;

class Label extends BasedBoard
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
        'board_id',
    ];

    /**
     * Card - m:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cards()
    {
        return $this->belongsToMany(Card::class, 'label_card', 'label_id', 'card_id');
    }
}
