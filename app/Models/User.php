<?php

namespace App\Models;

use App\Business\Authorization\AdminAccessible;
use App\Business\Authorization\ApiTokenAccessible;
use App\Business\Authorization\PasswordAccessible;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements AdminAccessible, PasswordAccessible, ApiTokenAccessible
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'api_token',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'is_admin',
    ];

    /**
     * Member - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(Member::class, 'user_id');
    }

    /**
     * Возвращает true, если пользователь является админом
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->id === 1;
    }

    /**
     * Возвращает true, если указанный пароль совпадает с текущим
     *
     * @param  string  $password
     * @return bool
     */
    public function passwordAsserts(string $password)
    {
        return Hash::check($password, $this->password);
    }

    /**
     * Сменяет пароль
     *
     * @param  string  $new_password
     * @return void
     */
    public function changePassword(string $new_password)
    {
        $this->password = $new_password;
        $this->save();
    }

    /**
     * Генерирует, записывает и возвращает токен API
     *
     * @return string
     */
    public function updateApiToken()
    {
        $this->api_token = mb_substr(sha1(microtime()) . md5(microtime()), rand(0, 11), 60);
        $this->save();

        return $this->api_token;
    }

    /**
     * Уничтожает токен API
     *
     * @return void
     */
    public function dropApiToken()
    {
        $this->api_token = null;
        $this->save();
    }

    /**
     * Задает пароль сразу в зашифрованном виде
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute(string $value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Возвращает признак того, является ли пользователь администратором
     *
     * @return bool
     */
    public function getIsAdminAttribute()
    {
        return $this->isAdmin();
    }
}
