<?php

namespace App\Models\Repositories;

use BadMethodCallException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Репозиторий пользователей
 */
class UserRepository extends Repository
{
    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \BadMethodCallException
     */
    public function update($key, Request $request)
    {
        if ($request->has('password')) {
            throw new BadMethodCallException(
                'The password change is not allowed here. Use "PATCH /user/password"'
            );
        }

        return parent::update($key, $request);
    }

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function destroy($key)
    {
        /** @var  \App\Business\Authorization\AdminAccessible|\Illuminate\Database\Eloquent\Model  $user */
        $user = $this->one($key);

        if ($user->isAdmin()) {
            throw new AccessDeniedHttpException('Unable to destroy an admin');
        }

        return $user->delete();
    }
}
