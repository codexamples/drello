<?php

namespace App\Models\Repositories\ManyToMany;

use App\Business\Data\Database\ManyToManyInterface;
use App\Support\Magic\MethodLikeField;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Реализация предоставления информации о моделе и ее связях "многие-ко-многим"
 *
 * @method  \Illuminate\Database\Eloquent\Model  getModel()
 * @method  array  getKeys()
 * @method  \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator  getRelatedModels()
 */
final class Object implements ManyToManyInterface, Arrayable
{
    use MethodLikeField;

    /**
     * Создает экземпляр
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  array  $keys
     * @param  \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator  $related_models
     */
    public function __construct($model, $keys, $related_models)
    {
        $this
            ->prop('getModel', $model)
            ->prop('getKeys', $keys)
            ->prop('getRelatedModels', $related_models);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'model' => $this->getModel(),
            'keys' => $this->getKeys(),
            'related' => $this->getRelatedModels(),
        ];
    }
}
