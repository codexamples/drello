<?php

namespace App\Models\Repositories\ManyToMany;

use App\Business\Repositories\ManyToManyRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Реализация репозитория связей "многие-ко-многим"
 */
final class Repository implements ManyToManyRepository
{
    /**
     * Возвращает модель
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function getModel(Request $request)
    {
        $model = '\App\Models\\' . studly_case($request->input('model'));
        return $model::findOrFail($request->input('key'));
    }

    /**
     * Возвращает связь
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    private function getRelation(Request $request, Model $model)
    {
        return call_user_func([$model, $request->input('relation')]);
    }

    /**
     * Возвращает список привязанных моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Database\ManyToManyInterface
     */
    public function get(Request $request)
    {
        $model = $this->getModel($request);
        $keys = $this->getRelation($request, $model)->pluck($request->input('related_identifier'))->toArray();
        $related_models = $this->getRelation($request, $model)->get();

        return new Object($model, $keys, $related_models);
    }

    /**
     * Синхронизирует привязки моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Database\ManyToManyInterface
     */
    public function sync(Request $request)
    {
        $model = $this->getModel($request);
        $this->getRelation($request, $model)->sync($request->input('keys'));

        return $this->get($request);
    }
}
