<?php

namespace App\Models\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий моделей Trello, принадлежащих доске
 */
class BasedBoardRepository extends TrelloModelRepository
{
    /**
     * Список внешних ключей
     *
     * @var array
     */
    protected $foreignKeys = [
        'board_id',
    ];

    /**
     * Готовит некоторые параметры запроса
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function prepareRequest(Request $request)
    {
        $request->merge(['url' => '']);
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $this->prepareRequest($request);

        return parent::create($request);
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \BadMethodCallException
     */
    public function update($key, Request $request)
    {
        $this->prepareRequest($request);

        return parent::update($key, $request);
    }
}
