<?php

namespace App\Models\Repositories;

use BadMethodCallException;
use Illuminate\Http\Request;

/**
 * Репозиторий моделей Trello
 */
class TrelloModelRepository extends Repository
{
    /**
     * Список внешних ключей
     *
     * @var array
     */
    protected $foreignKeys = [];

    /**
     * Стартует запрос для выборки всех моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function queryAll(Request $request = null)
    {
        $q = parent::queryAll($request);

        if ($request && $this->foreignKeys) {
            foreach ($this->foreignKeys as $key) {
                if ($request->has($key)) {
                    $q->where($key, $request->input($key));
                }
            }
        }

        return $q;
    }

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request)
    {
        $request->merge(['id' => $request->input('_id')]);

        return parent::create($request);
    }

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \BadMethodCallException
     */
    public function update($key, Request $request)
    {
        if ($request->has('_id')) {
            throw new BadMethodCallException('Unable to change this model\'s id');
        }

        return parent::update($key, $request);
    }
}
