<?php

namespace App\Models\Repositories;

/**
 * Репозиторий карточек
 */
class CardRepository extends TrelloModelRepository
{
    /**
     * Список внешних ключей
     *
     * @var array
     */
    protected $foreignKeys = [
        'stage_list_id',
    ];
}
