<?php

namespace App\Models\Repositories;

/**
 * Репозиторий досок
 */
class BoardRepository extends TrelloModelRepository
{
    /**
     * Список внешних ключей
     *
     * @var array
     */
    protected $foreignKeys = [
        'organisation_id',
    ];
}
