<?php

namespace App\Models;

class StageList extends BasedBoard
{
    use HasClosedField;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
        'closed',
        'position',
        'board_id',
    ];

    /**
     * Card - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards()
    {
        return $this->hasMany(Card::class, 'stage_list_id');
    }
}
