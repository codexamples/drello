<?php

namespace App\Models;

class Board extends TrelloModel
{
    use HasClosedField;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
        'closed',
        'organisation_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Organisation - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }

    /**
     * StageList - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stageLists()
    {
        return $this->hasMany(StageList::class, 'board_id');
    }

    /**
     * Label - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function labels()
    {
        return $this->hasMany(Label::class, 'board_id');
    }
}
