<?php

namespace App\Models;

class Organisation extends TrelloModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
        'display_name',
    ];

    /**
     * Board - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boards()
    {
        return $this->hasMany(Board::class, 'organisation_id');
    }
}
