<?php

namespace App\Models\Wizard;

use App\Business\Data\Wizard\SetBoardsReport as SetBoardsReportContract;
use App\Support\Magic\MethodLikeField;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Реализация отчета о добавленных моделях
 *
 * @method  int  getAllCount()
 * @method  int  getBoardCount()
 * @method  int  getStageListCount()
 * @method  int  getCardCount()
 * @method  int  getLabelCount()
 * @method  int  getMemberCount()
 * @method  int  getWebhookCount()
 */
class SetBoardsReport implements SetBoardsReportContract, Arrayable
{
    use MethodLikeField;

    /**
     * Создает новый экземпляр
     *
     * @param  int  $boards
     * @param  int  $stage_lists
     * @param  int  $cards
     * @param  int  $labels
     * @param  int  $members
     */
    public function __construct(int $boards, int $stage_lists, int $cards, int $labels, int $members)
    {
        $this
            ->prop('getBoardCount', $boards)
            ->prop('getStageListCount', $stage_lists)
            ->prop('getCardCount', $cards)
            ->prop('getLabelCount', $labels)
            ->prop('getMemberCount', $members)
            ->prop('getWebhookCount', 0)
            ->prop('getAllCount', $boards + $stage_lists + $cards + $labels + $members + 1);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'all' => $this->getAllCount(),
            'models' => [
                'boards' => $this->getBoardCount(),
                'stage_lists' => $this->getStageListCount(),
                'cards' => $this->getCardCount(),
                'labels' => $this->getLabelCount(),
                'members' => $this->getMemberCount(),
                'webhooks' => $this->getWebhookCount(),
            ],
        ];
    }
}
