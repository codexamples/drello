<?php

namespace App\Models\Wizard;

use App\Business\Data\Wizard\ReadyState as ReadyStateContract;
use App\Support\Magic\MethodLikeField;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Реализация информации о готовности настроек
 *
 * @method  bool  isFullyReady()
 * @method  bool  areCredentialsReady()
 * @method  bool  wereBoardsImported()
 */
class ReadyState implements ReadyStateContract, Arrayable
{
    use MethodLikeField;

    /**
     * Создает новый экземпляр
     *
     * @param  bool  $credentials
     * @param  bool  $boards_imported
     */
    public function __construct(bool $credentials, bool $boards_imported)
    {
        $this
            ->prop('areCredentialsReady', $credentials)
            ->prop('wereBoardsImported', $boards_imported)
            ->prop('isFullyReady', $credentials && $boards_imported);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'fully' => $this->isFullyReady(),
            'steps' => [
                'credentials' => $this->areCredentialsReady(),
                'boards' => $this->wereBoardsImported(),
            ],
        ];
    }
}
