<?php

namespace App\Models\Wizard;

use App\Business\Data\Wizard\Wizard as WizardContract;
use App\Business\Repositories\ManyToManyRepository;
use App\Jobs\GetTrelloBoards;
use App\Models\Board;
use App\Models\Repositories\BoardRepository;
use App\Models\Repositories\CardRepository;
use App\Models\Repositories\LabelRepository;
use App\Models\Repositories\MemberRepository;
use App\Models\Repositories\OrganisationRepository;
use App\Models\Repositories\StageListRepository;
use App\Services\Trello\HasTrello;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * Реализация настройщика
 */
class Wizard implements WizardContract
{
    use HasTrello;

    /**
     * Возвращает информацию о готовности настроек
     *
     * @return \App\Business\Data\Wizard\ReadyState
     */
    public function determineReadyState()
    {
        $credentials = $this->trello()->apiKey() && $this->trello()->apiToken();
        $boards_imported = Board::count() > 0;

        return new ReadyState($credentials, $boards_imported);
    }

    /**
     * Возвращает ключ от Trello Api
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->trello()->apiKey();
    }

    /**
     * Возвращает токен от Trello Api
     *
     * @return string
     */
    public function getApiToken()
    {
        return $this->trello()->apiToken();
    }

    /**
     * Задает реквизиты авторизации в Trello Api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     */
    public function setCredentials(Request $request)
    {
        $key = $this->getApiKey();
        $token = $this->getApiToken();

        $this->trello()
            ->apiKey($request->input('key'))
            ->apiToken($request->input('token'));

        try {
            $this->trello()->getWebhooks();
        } catch (Exception $ex) {
            $this->trello()
                ->apiKey($key)
                ->apiToken($token);

            throw new InvalidParameterException('Invalid credentials');
        }
    }

    /**
     * Стартует джоб на выборку всех данных
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function getBoards(Request $request)
    {
        dispatch(new GetTrelloBoards($request->input('organisation')));

        return $request->input('organisation');
    }

    /**
     * Берет из Trello Api весь стафф по организации
     *
     * @param  string  $organisation_id
     * @return void
     */
    public function endGetBoards(string $organisation_id)
    {
        $data = $this->trello()->manager()->getOrganization($organisation_id)->getData();
        $organisation = [
            '_id' => $data['id'],
            'name' => $data['name'],
            'url' => $data['url'],
            'display_name' => $data['displayName'],
            'boards' => [],
        ];

        $members = [];
        $labels = [];

        foreach ($this->trello()->manager()->getOrganization($organisation['_id'])->getBoards() as $board) {
            $data = $board->getData();
            $board = [
                '_id' => $data['id'],
                'name' => $data['name'],
                'url' => $data['url'],
                'closed' => $data['closed'],
                'organisation_id' => $organisation['_id'],
                'stage_lists' => [],
            ];

            foreach ($data['lists'] as $list) {
                $stage_list = [
                    '_id' => $list['id'],
                    'name' => $list['name'],
                    'url' => '',
                    'closed' => $list['closed'],
                    'position' => $list['pos'],
                    'board_id' => $board['_id'],
                    'cards' => [],
                ];

                foreach ($this->trello()->manager()->getList($stage_list['_id'])->getCards() as $card) {
                    $data = $card->getData();
                    $card = [
                        '_id' => $data['id'],
                        'name' => $data['name'],
                        'url' => $data['url'],
                        'due' => $data['badges']['due'] ? date('Y-m-d H:i:s', strtotime($data['badges']['due'])) : null,
                        'closed' => $data['closed'],
                        'position' => $data['pos'],
                        'labels' => [],
                        'stage_list_id' => $stage_list['_id'],
                        'members' => [],
                    ];

                    foreach ($data['labels'] as $label) {
                        if (!array_key_exists($label['id'], $labels)) {
                            $labels[$label['id']] = [
                                '_id' => $label['id'],
                                'name' => $label['name'],
                                'url' => '',
                                'board_id' => $board['_id'],
                            ];
                        }

                        $card['labels'][] = $label['id'];
                    }

                    foreach ($data['members'] as $member) {
                        if (!array_key_exists($member['id'], $members)) {
                            $members[$member['id']] = [
                                '_id' => $member['id'],
                                'name' => $member['fullName'],
                                'url' => $this->trello()->manager()->getMember($member['id'])->getUrl(),
                                'initials' => $member['initials'],
                                'tag' => '',
                                'user_id' => null,
                            ];
                        }

                        $card['members'][] = $member['id'];
                    }

                    $stage_list['cards'][] = $card;
                }

                $board['stage_lists'][] = $stage_list;
            }

            $organisation['boards'][] = $board;
        }

        $result = json_encode([
            'key' => $organisation_id,
            'organisation' => $organisation,
            'members' => $members,
            'labels' => $labels,
        ]);

        Redis::set('trello:' . $organisation_id, $result);
        Redis::publish('trello', $result);
    }

    /**
     * Заполняет ранее добавленными моделями
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Wizard\SetBoardsReport
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function setBoards(Request $request)
    {
        $redis_key = 'trello:' . $request->input('organisation');
        $data = json_decode(Redis::get($redis_key), true);

        if (!is_array($data)) {
            throw new ModelNotFoundException('Organisation not found: ' . $request->input('organisation'));
        }

        // normalize

        $organisation = $data['organisation'];
        $members = $data['members'];
        $labels = $data['labels'];
        $boards = [];
        $stage_lists = [];
        $cards = [];

        foreach ($organisation['boards'] as $board) {
            $boards[] = $board;

            foreach ($board['stage_lists'] as $stage_list) {
                $stage_lists[] = $stage_list;

                foreach ($stage_list['cards'] as $card) {
                    // agooteens
                    if (mb_strlen($card['url']) > 191) {
                        $card['url'] = mb_substr($card['url'], 0, 191);
                    }
                    // end of agooteens

                    $cards[] = $card;
                }
            }
        }

        // organisation

        /** @var  \App\Business\Repositories\ModelRepository  $models */
        /** @var  \App\Business\Repositories\ManyToManyRepository  $many */

        $models = app()->make(OrganisationRepository::class);
        $many = app()->make(ManyToManyRepository::class);

        $request = new Request();
        unset($organisation['boards']);

        $request->merge($organisation);
        $models->create($request);

        // boards

        $models = app()->make(BoardRepository::class);

        foreach ($boards as $board) {
            $request = new Request();
            unset($board['stage_lists']);

            $request->merge($board);
            $models->create($request);
        }

        // stage lists

        $models = app()->make(StageListRepository::class);

        foreach ($stage_lists as $stage_list) {
            $request = new Request();
            unset($stage_list['cards']);

            $request->merge($stage_list);
            $models->create($request);
        }

        // labels

        $models = app()->make(LabelRepository::class);

        foreach ($labels as $label) {
            $request = new Request();

            $request->merge($label);
            $models->create($request);
        }

        // members

        $models = app()->make(MemberRepository::class);

        foreach ($members as $member) {
            $request = new Request();

            $request->merge($member);
            $models->create($request);
        }

        // cards

        $models = app()->make(CardRepository::class);

        foreach ($cards as $card) {
            $request = new Request();
            $label_ids = $card['labels'];
            $member_ids = $card['members'];

            unset($card['labels']);
            unset($card['members']);

            $request->merge($card);
            $models->create($request);

            // many-to-many

            $request = new Request();
            $request->merge([
                'model' => 'card',
                'key' => $card['_id'],
                'related_identifier' => 'id',
            ]);

            $request->merge([
                'relation' => 'labels',
                'keys' => $label_ids,
            ]);
            $many->sync($request);

            $request->merge([
                'relation' => 'members',
                'keys' => $member_ids,
            ]);
            $many->sync($request);
        }

        // finish

        Redis::del($redis_key);

        return new SetBoardsReport(
            count($boards),
            count($stage_lists),
            count($cards),
            count($labels),
            count($members)
        );
    }
}
