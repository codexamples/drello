<?php

namespace App\Models;

class Card extends TrelloModel
{
    use HasClosedField;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'url',
        'due',
        'closed',
        'position',
        'stage_list_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * StageList - 1:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stageList()
    {
        return $this->belongsTo(StageList::class, 'stage_list_id');
    }

    /**
     * Member - m:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(Member::class, 'member_card', 'card_id', 'member_id');
    }

    /**
     * Label - m:m
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels()
    {
        return $this->belongsToMany(Label::class, 'label_card', 'card_id', 'label_id');
    }
}
