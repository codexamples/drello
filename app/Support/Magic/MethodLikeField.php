<?php

namespace App\Support\Magic;

use Illuminate\Container\EntryNotFoundException;

/**
 * Хранение полей класса, как методов
 * Доступ к полям через магию
 */
trait MethodLikeField
{
    /**
     * Свойства класса
     *
     * @var array
     */
    private $props = [];

    /**
     * Задает новое свойство класса
     *
     * @param  string  $k
     * @param  $v
     * @return static
     */
    protected function prop(string $k, $v)
    {
        $this->props[$k] = $v;

        return $this;
    }

    /**
     * Вызывает метод через магию
     *
     * @param  string  $method
     * @param  array  $args
     * @return mixed
     * @throws \Illuminate\Container\EntryNotFoundException
     */
    public function __call(string $method, array $args)
    {
        if (array_key_exists($method, $this->props)) {
            return $this->props[$method];
        }

        throw new EntryNotFoundException('Method not found: ' . $method);
    }
}
