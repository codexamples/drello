<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Business\Data\Preferences\Settings::class,
            \App\Models\Preferences\Settings::class
        );

        $this->app->bind(
            \App\Business\Repositories\ManyToManyRepository::class,
            \App\Models\Repositories\ManyToMany\Repository::class
        );

        $this->app->bind(
            \App\Business\Services\Trello\Trello::class,
            \App\Services\Trello\Trello::class
        );

        $this->app->bind(
            \App\Business\Data\Wizard\Wizard::class,
            \App\Models\Wizard\Wizard::class
        );
    }
}
