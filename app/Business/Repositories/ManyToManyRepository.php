<?php

namespace App\Business\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий связей "многие-ко-многим"
 */
interface ManyToManyRepository
{
    /**
     * Возвращает список привязанных моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Database\ManyToManyInterface
     */
    public function get(Request $request);

    /**
     * Синхронизирует привязки моделей
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Database\ManyToManyInterface
     */
    public function sync(Request $request);
}
