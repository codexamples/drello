<?php

namespace App\Business\Repositories;

use Illuminate\Http\Request;

/**
 * Репозиторий моделей БД
 */
interface ModelRepository
{
    /**
     * Возвращает все записи модели, или с пагинацией
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all(Request $request = null);

    /**
     * Возвращает запись модели по указанному значению первичного ключа
     *
     * @param  $key
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function one($key);

    /**
     * Создает новую запись в моделе, возвращает ее экземпляр
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Request $request);

    /**
     * Обновляет данные записи модели, возвращает ее экземпляр
     *
     * @param  $key
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($key, Request $request);

    /**
     * Удаляет запись модели
     *
     * @param  $key
     * @return bool|null
     */
    public function destroy($key);
}
