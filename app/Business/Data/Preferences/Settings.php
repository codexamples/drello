<?php

namespace App\Business\Data\Preferences;

/**
 * Доступ к секциям настроек
 */
interface Settings
{
    /**
     * Создает и возвращает экземпляр секции настроек по указанному ключу
     *
     * @param  string  $key
     * @param  $data
     * @return \App\Business\Data\Preferences\Section
     * @throws \App\Business\Data\Preferences\SectionNotFoundException
     */
    public function makeSection(string $key, $data);

    /**
     * Загружает и возвращает экземпляр секции настроек по указанному ключу
     *
     * @param  string  $key
     * @return \App\Business\Data\Preferences\Section
     */
    public function loadSection(string $key);

    /**
     * Сохраняет значения секции
     *
     * @param  \App\Business\Data\Preferences\Section  $section
     * @return static
     */
    public function saveSection(Section $section);

    /**
     * Удаляет все значения секции по указанному ключу
     *
     * @param  string  $key
     * @return static
     */
    public function removeSection(string $key);
}
