<?php

namespace App\Business\Data\Preferences;

/**
 * Секция из настроек
 */
interface Section
{
    /**
     * Возвращает название ключа секции
     *
     * @return string
     */
    public function getSectionKey();

    /**
     * Собирает все значения секции в одну сериализованную строку
     *
     * @return string
     */
    public function compile();

    /**
     * Считывает значение из секции по указанному адресу
     *
     * @param  string  $offset
     * @return mixed
     */
    public function read(string $offset);

    /**
     * Считывает все значения из секции
     *
     * @return array
     */
    public function readAll();

    /**
     * Записывает значение в секцию по указанному адресу
     *
     * @param  string  $offset
     * @param  $value
     * @return static
     */
    public function write(string $offset, $value);

    /**
     * Записывает значения в секцию из массива
     *
     * @param  array  $data
     * @return static
     */
    public function fill(array $data);

    /**
     * Удаляет значение из секции
     *
     * @param  string  $offset
     * @return static
     */
    public function remove(string $offset);
}
