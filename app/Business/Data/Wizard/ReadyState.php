<?php

namespace App\Business\Data\Wizard;

/**
 * Информация о готовности настроек
 *
 * @method  bool  isFullyReady()
 * @method  bool  areCredentialsReady()
 * @method  bool  wereBoardsImported()
 */
interface ReadyState
{
}
