<?php

namespace App\Business\Data\Wizard;

use Illuminate\Http\Request;

/**
 * Настройщик
 */
interface Wizard
{
    /**
     * Возвращает информацию о готовности настроек
     *
     * @return \App\Business\Data\Wizard\ReadyState
     */
    public function determineReadyState();

    /**
     * Возвращает ключ от Trello Api
     *
     * @return string
     */
    public function getApiKey();

    /**
     * Возвращает токен от Trello Api
     *
     * @return string
     */
    public function getApiToken();

    /**
     * Задает реквизиты авторизации в Trello Api
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function setCredentials(Request $request);

    /**
     * Стартует джоб на выборку всех данных
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function getBoards(Request $request);

    /**
     * Берет из Trello Api весь стафф по организации
     *
     * @param  string  $organisation_id
     * @return void
     */
    public function endGetBoards(string $organisation_id);

    /**
     * Заполняет ранее добавленными моделями
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Business\Data\Wizard\SetBoardsReport
     */
    public function setBoards(Request $request);
}
