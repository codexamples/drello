<?php

namespace App\Business\Data\Wizard;

/**
 * Отчет о добавленных моделях
 *
 * @method  int  getAllCount()
 * @method  int  getBoardCount()
 * @method  int  getStageListCount()
 * @method  int  getCardCount()
 * @method  int  getLabelCount()
 * @method  int  getMemberCount()
 * @method  int  getWebhookCount()
 */
interface SetBoardsReport
{
}
