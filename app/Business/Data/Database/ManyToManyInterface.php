<?php

namespace App\Business\Data\Database;

/**
 * Информация о моделе и ее связях "многие-ко-многим"
 *
 * @method  \Illuminate\Database\Eloquent\Model  getModel()
 * @method  array  getKeys()
 * @method  \Illuminate\Database\Eloquent\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator  getRelatedModels()
 */
interface ManyToManyInterface
{
}
