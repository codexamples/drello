<?php

namespace App\Business\Services\Trello;

use Illuminate\Http\Request;

/**
 * Обертка над Trello-библиотекой
 */
interface Trello
{
    /**
     * Возвращает или задает ключ от Trello Api
     *
     * @param  $value
     * @return string|static
     */
    public function apiKey($value = null);

    /**
     * Возвращает или задает токен от Trello Api
     *
     * @param  $value
     * @return string|static
     */
    public function apiToken($value = null);

    /**
     * Возвращает клиента Trello
     *
     * @return \Trello\Client
     */
    public function client();

    /**
     * Возвращает манагера клиента Trello
     *
     * @return \Trello\Manager
     */
    public function manager();

    /**
     * Возвращает вебхук(-и)
     *
     * @param  string  $id
     * @return array
     */
    public function getWebhooks(string $id = '');

    /**
     * Создает вебхук
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function setWebhook(Request $request);

    /**
     * Удаляет вебхук
     *
     * @param  string  $id
     * @return array
     */
    public function dropWebhook(string $id);

    /**
     * Обрабатывает вебхуки, сгенеренные из Trello
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function handleWebhook(Request $request);
}
