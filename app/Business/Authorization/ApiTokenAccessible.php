<?php

namespace App\Business\Authorization;

/**
 * Функционал по работе с токенами API
 */
interface ApiTokenAccessible
{
    /**
     * Генерирует, записывает и возвращает токен API
     *
     * @return string
     */
    public function updateApiToken();

    /**
     * Уничтожает токен API
     *
     * @return void
     */
    public function dropApiToken();
}
