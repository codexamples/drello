<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->string('id');
            $table->string('name');
            $table->string('url');
            $table->dateTime('due')->nullable();
            $table->boolean('closed');
            $table->double('position');
            $table->string('stage_list_id');
            $table->timestamps();

            $table->primary('id');
            $table->foreign('stage_list_id')->references('id')->on('stage_lists')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
