<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_lists', function (Blueprint $table) {
            $table->string('id');
            $table->string('name');
            $table->string('url');
            $table->boolean('closed');
            $table->double('position');
            $table->string('board_id');
            $table->timestamps();

            $table->primary('id');
            $table->foreign('board_id')->references('id')->on('boards')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_lists');
    }
}
