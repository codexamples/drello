<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API v1 Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::put('/', 'AuthController@login')->middleware('guest:api');
    Route::delete('/', 'AuthController@logout')->middleware('auth:api');
});

/*
|--------------------------------------------------------------------------
| Bearer Token Protected
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    // ====================================================================
    // Board
    // ====================================================================
    Route::group(['prefix' => 'board'], function () {
        Route::get('/', 'BoardController@index');
        Route::get('/{id}', 'BoardController@view');
        Route::get('/{id}/stage-lists', 'BoardController@stageLists');
        Route::get('/{id}/labels', 'BoardController@labels');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'BoardController@store');
            Route::patch('/{id}', 'BoardController@update');
            Route::delete('/{id}', 'BoardController@destroy');
        });
    });
    // ====================================================================
    // Card
    // ====================================================================
    Route::group(['prefix' => 'card'], function () {
        Route::get('/', 'CardController@index');
        Route::get('/{id}', 'CardController@view');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'CardController@store');
            Route::patch('/{id}', 'CardController@update');
            Route::delete('/{id}', 'CardController@destroy');
        });
    });
    // ====================================================================
    // Many-To-Many
    // ====================================================================
    Route::group(['prefix' => 'many-to-many'], function () {
        Route::get('/', 'ManyToManyController@index');
        Route::post('/', 'ManyToManyController@sync')->middleware('admin');
    });
    // ====================================================================
    // Label
    // ====================================================================
    Route::group(['prefix' => 'label'], function () {
        Route::get('/', 'LabelController@index');
        Route::get('/{id}', 'LabelController@view');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'LabelController@store');
            Route::patch('/{id}', 'LabelController@update');
            Route::delete('/{id}', 'LabelController@destroy');
        });
    });
    // ====================================================================
    // Member
    // ====================================================================
    Route::group(['prefix' => 'member'], function () {
        Route::get('/', 'MemberController@index');
        Route::get('/{id}', 'MemberController@view');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'MemberController@store');
            Route::patch('/{id}', 'MemberController@update');
            Route::delete('/{id}', 'MemberController@destroy');
        });
    });
    // ====================================================================
    // Organisation
    // ====================================================================
    Route::group(['prefix' => 'organisation'], function () {
        Route::get('/', 'OrganisationController@index');
        Route::get('/{id}', 'OrganisationController@view');
        Route::get('/{id}/boards', 'OrganisationController@boards');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'OrganisationController@store');
            Route::patch('/{id}', 'OrganisationController@update');
            Route::delete('/{id}', 'OrganisationController@destroy');
        });
    });
    // ====================================================================
    // Stage List
    // ====================================================================
    Route::group(['prefix' => 'stage-list'], function () {
        Route::get('/', 'StageListController@index');
        Route::get('/{id}', 'StageListController@view');
        Route::get('/{id}/cards', 'StageListController@cards');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'StageListController@store');
            Route::patch('/{id}', 'StageListController@update');
            Route::delete('/{id}', 'StageListController@destroy');
        });
    });
    // ====================================================================
    // User
    // ====================================================================
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@index');
        Route::get('/{id}', 'UserController@view');
        Route::patch('/password', 'UserController@password');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'UserController@store');
            Route::patch('/{id}', 'UserController@update');
            Route::delete('/{id}', 'UserController@destroy');
            Route::patch('/{id}/password', 'UserController@passwordForced');
        });
    });
    // ====================================================================
    // Webhook Model
    // ====================================================================
    Route::group(['prefix' => 'webhook-model'], function () {
        Route::get('/', 'WebhookModelController@index');
        Route::get('/{id}', 'WebhookModelController@view');

        Route::group(['middleware' => 'admin'], function () {
            Route::put('/', 'WebhookModelController@store');
            Route::delete('/{id}', 'WebhookModelController@destroy');
        });
    });
    // ====================================================================
    // Wizard
    // ====================================================================
    Route::group(['prefix' => 'wizard'], function () {
        Route::get('/', 'WizardController@checkReady');

        Route::group(['prefix' => 'credentials'], function () {
            Route::get('/', 'WizardController@getCredentials');
            Route::post('/', 'WizardController@setCredentials')->middleware('admin');
        });

        Route::group([
            'prefix' => 'boards',
            'middleware' => 'admin',
        ], function () {
            Route::post('/', 'WizardController@getBoards');
            Route::put('/', 'WizardController@setBoards');
        });
    });
});

/*
|--------------------------------------------------------------------------
| Trello Webhooking
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => 'webhook'], function () {
    Route::get('/', 'WebhookController@check');
    Route::post('/', 'WebhookController@handle');
});
